package hashing;

public class Hashing {
	/*
	 * Respuesta de las preguntas
	 * a)¿Que ocurre con las funciones de insercion, busqueda y eliminacion cuando
	 * el factor de
	 * carga es cercano a 1?
	 * - Insercion: A medida que aumente el factor, esto hara que tambien aumente el
	 * tiempo que que lo tomara,
	 * debido a que el metodo le tomara mas tiempo en buscar el pespacio para
	 * insertar.
	 * - Busqueda: a medida que el factor de carga vaya aumentando, el tiempo que se
	 * tardara en buscar tambien lo hara
	 * ya que hay mas datos que recorrer para buscar el nodo.
	 * - Eliminacion: Mientras el factor de carga vaya aumentando, la funcion
	 * permanecera igual.
	 * b)Describir los pasos a realizar para duplicar el tamaño de la tabla hash y
	 * reducir
	 * el factor de carga. ¿Para que valor de "alfa" es necesari para realizar el
	 * proceso?
	 * - Lo primero que se realizaria seria el metodo de insercion , este comprueba
	 * si el factor de carga es 1 luego de insertar el nodo y si es asi,
	 * se duplicara el tamaño de la tabla hash, ingresando el duplicado en otro
	 * arreglo donde tendra el doble de tamanio, por esto es que el valor del factor
	 * de
	 * carga siempre sera 0
	 */
	class Nodo {
		int key;

		public Nodo(int k) {
			key = k;
		}
	}

	Nodo[] T;

	public Hashing(int m) {
		T = new Nodo[m];
		for (int i = 0; i < m; i++)
			T[i] = null;
	}

	// Implementar
	// return true si fue posible insertar
	// false en otro caso
	public boolean insertar(int key) {
		int i = key % T.length;
		while (T[i] != null) {
			i = (i + 1) % T.length;
		}
		if (T[i] == null) {
			T[i] = new Nodo(key);
			return true;
		} else {
			return false;
		}
	}

	// Implementar
	// Si la clave no existe, return null
	public Nodo buscar(int key) {
		int i = key % T.length;
		while (T[i] != null) {
			if (T[i].key == key) {
				return T[i];
			}
			i = (i + 1) % T.length;
		}
		return null;
	}

	// Implementar
	// return true si fue posible eliminar
	// false en otro caso
	public boolean eliminar(int key) {
		int i = key % T.length;
		while (T[i] != null) {
			if (T[i].key == key) {
				T[i] = null;
				return true;
			}
		}
		return false;
	}

	// Implementar el metodo para que calcula el factor de carga del hashing
	public double factorCarga() {
		int cantidad = 0;
		for (int i = 0; i < T.length; i++) {
			if (T[i] != null) {
				cantidad++;
			}
		}
		return (double) cantidad / T.length;
	}

	int h(int k, int i) {
		return (h1(k) + (i * h2(k))) % T.length;
	}

	int h1(int k) {
		return k % T.length;
	}

	int h2(int k) {
		return 1 + (k % (T.length - 1));
	}

}
