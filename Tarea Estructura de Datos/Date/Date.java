package Date;

public class Date {
    private int fecha;

    public Date(){
        fecha=0;
    }
    //Devuelve el Año desplazando los bits
    public int getAnio(){
        int año = fecha >>> 20;
        return anio;
    }
    //Devuelve el mes moviendo los bits al inicio para no contar los bits del año
    //Luego los desplaza de vuelta para regresar el dato del mes
    public int getMes(){
        int mes = (fecha << 12) >>> 28;
        return mes;
    }
    //Devuelve el dia moviendo los bits al inicio para no contar los bits del año
    //Luego los desplaza de vuelta para regresar el dia
    public int getDia(){
        int dia = (fecha << 16) >>> 27;
        return dia;
    }
    //Ingresa el año para la fecha
    public void setAño(int año){
        //Revisa si el año ingresado es posible en base a la cantida de bits.
        if(año>=0 && año<4096){
            //Coloca los datos binarios del año en la fecha, eliminando la anterior usando una mascara
            int mask=1048575;//00000000000011111111111111111111
            fecha = (fecha & mask) | (año << 20);
        }else{
            System.out.println("Año ingreasado no es valido");
        }
    }
    //Cambia el mes de la fecha
    public void setMes(int mes){
        //Revisa si el mes ingresado es valido en base a la cantida de bits.
        if(mes>=1 && mes<13){
            //Agrega los bits del mes en la fecha, borrando la anterior usando una mascara
            int mask=-983041;//11111111111100001111111111111111
            fecha = (fecha & mask) | (mes << 16);
        }else{
            System.out.println("Mes ingresado no es valido");
        }
    }
    //Cambia el dia en la fecha ingresada
    public void setDia(int dia){
        //Revisa si el dia es valido en base a la cantidad de bits.
        if(dia>=1 && dia<32){
            //Ingresa los bits en la fecha, eliminando la anterior agregada
            //A traves de una mascara
            int mask =-63489;//11111111111111110000011111111111
            fecha = (fecha & mask) | (dia << 11);
        }else{
            System.out.println("Dia ingresado no valido");
        }

    }
    //Imprime la fecha en formato String
    @Override
    public String toString() {
        if(getMes()<10){
            return getDia() + "/0" + getMes() + "/" + getAño();
        }else{
            return getDia() + "/" + getMes() + "/" + getAño();
        }
    }

}
