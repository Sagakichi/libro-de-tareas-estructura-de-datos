package ArbolDeExpresion;

import java.util.Queue;
import java.util.Stack;

/**
 * 
 * @author Martita
 * 
 *         Para m�s info, visitar
 *         https://es.stackoverflow.com/questions/26907/infijo-a-posfijo-en-java
 *         Y http://informatica.uv.es/iiguia/AED/laboratorio/P6/pr_06_2005.html
 *
 */

public class Parsing {

	public static String fromInfijoToPostfijo(String infijo) {
		String result = "";
		Stack<Character> Pila = new Stack<>();
		char[] tokens = infijo.toCharArray();
		for (char x : tokens) {
			if (isANumber(x)) {
				result += x;
			} else if (prioridad(x) == 0) {
				Pila.push(x);
			} else if (prioridad(x) == -1) {
				while (Pila.peek() != '(') {
					result += Pila.pop();
				}
				Pila.pop();
			} else if (prioridad(x) == 1 || prioridad(x) == 2) {
				if (Pila.isEmpty()) {
					Pila.push(x);
				} else {
					if (prioridad(x) > prioridad(Pila.peek())) {
						Pila.push(x);
					} else {
						result += Pila.pop();
						Pila.push(x);
					}
				}
			}
		}
		while (!Pila.isEmpty()) {
			result += Pila.pop();
		}
		return result;
	}

	private static boolean isANumber(char symbol) {
		return symbol == '0' || symbol == '1' || symbol == '2' || symbol == '3' ||
				symbol == '4' || symbol == '5' || symbol == '6' || symbol == '7' ||
				symbol == '8' || symbol == '9';
	}

	private static int prioridad(char symbol) {
		if (symbol == '(')
			return 0;
		if (symbol == '+' || symbol == '-')
			return 1;
		if (symbol == '*' || symbol == '/')
			return 2;
		return -1;
	}

	public static ArbolExpresion getArbol(String postfijo) {
		Stack<ArbolExpresion> Pila = new Stack<>();

		char[] tokens = postfijo.toCharArray();
		for (char s : tokens) {
			ArbolExpresion A = new ArbolExpresion(s);
			if (isANumber(s)) {
				Pila.push(A);
			} else {
				A.left = Pila.pop();
				A.right = Pila.pop();
				Pila.push(A);
			}
		}
		return Pila.peek();
	}

	public static int Calculator(ArbolExpresion x) {
		if (x == null) {
			return 0;
		}
		int valor = Calculator(x.right);
		int result = 0;
		if (isANumber(x.symbol)) {
			result = Character.getNumericValue(x.symbol);
		} else if (prioridad(x.symbol) == 1 || prioridad(x.symbol) == 2) {
			switch (x.symbol) {
				case '+':
					result = valor + Calculator(x.left);
					break;
				case '-':
					result = valor - Calculator(x.left);
					break;
				case '*':
					result = valor * Calculator(x.left);
					break;
				case '/':
					result = valor / Calculator(x.left);
					break;
			}
		}
		return result;
	}
}
