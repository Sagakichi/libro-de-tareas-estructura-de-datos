package ListaEnlazada;

import java.util.Scanner;

public class TestLista {
    public static void main(String[] args) {
        Lista l = new Lista();
        Scanner teclado = new Scanner(System.in);
        int numero;
        int data;
        System.out.println("Ingrese la cantidad de datos a obtener");
        numero = 5;
        l.InsertaFinal(7);
        l.InsertaInicio(19);
        l.InsertaFinal(32);
        l.InsertaInicio(41);
        l.InsertaFinal(53);
        System.out.println("Se han ingresado los siguientes datos:");
        l.Print();
        int promedio = l.Promedio();
        System.out.println("\nEl promedio de la lista es el siguiente:" + promedio);
        Lista x = l.GetPromedioMayores(promedio);
        System.out.println("Los datos que son mayores al promedio de la lista son:");
        x.Print();

    }
}
