package ListaEnlazada;

public class Lista {
    private Nodo laCabeza;

    Lista() {
        laCabeza = null;
    }

    // ---Inserta un objeto(int) al comienzo de la lista
    public void InsertaInicio(int o) {
        if (EstaVacia()) {
            laCabeza = new Nodo(o, null);
        } else {
            laCabeza = new Nodo(o, laCabeza);
        }
    }

    // ---- Inserta al final ----
    public void InsertaFinal(int o) {
        if (EstaVacia()) {
            laCabeza = new Nodo(o, null);
        } else {
            Nodo t;
            for (t = laCabeza; t.next != null; t = t.next)
                ;
            ;
            t.next = new Nodo(o, null);
        }
    }

    // Hace que los datos sean ingresados a la lista en orden menor a mayor
    public void InsertaEnOrden(int o) {
        if (EstaVacia()) {
            laCabeza = new Nodo(o, null);
        } else if (laCabeza.elObjeto > o) {
            // Se toma condicion en donde si el datos es menor se ingresa al inicio de la
            // lista enlazada
            InsertaInicio(o);
        } else {
            Nodo n = laCabeza;
            Nodo a = new Nodo(o, null);
            while (n.next != null) {
                if (n.next.elObjeto > a.elObjeto) {
                    a.next = n.next;
                    n.next = a;
                    return;
                }
                n = n.next;
            }
            if (n.next == null) {
                n.next = a;
            }
        }
    }

    // ---cuenta la cantidad de nodos de la lista (Size)
    public int Size() {
        int tnodos = 0;
        for (Nodo t = laCabeza; t != null; t = t.next) {
            tnodos++;
        }
        return tnodos;
    }

    public void Eliminar(int o) {
        if (!EstaVacia()) {
            if (laCabeza.elObjeto == o) {
                laCabeza = laCabeza.next;
            } else {
                Nodo p = laCabeza;
                Nodo t = laCabeza.next;
                while (t != null && t.elObjeto != o) {
                    p = t;
                    t = t.next;
                }
                if (t.elObjeto == o) {
                    p.next = t.next;
                }
            }
        }
    }

    // Metodo que busca calcular el promedio de los datos ingresados a la lista
    public int Promedio() {
        Nodo n = laCabeza;
        int cant = 0, tamaño = Size();
        while (n != null) {
            cant += n.elObjeto;
            n = n.next;
        }
        if (tamaño == 0) {
            return 0;
        } else {
            return cant / tamaño;
        }
    }

    // Metodo que creara una lista en donde se guardaran solo los datos de la lista
    // anterior que son mayores al promedio antes calculado
    public Lista GetPromedioMayores(int promedio) {
        Lista prom = new Lista();
        Nodo n = laCabeza;
        while (n != null) {
            if (n.elObjeto > promedio) {
                prom.InsertaEnOrden(n.elObjeto);
            }
            n = n.next;
        }
        return prom;
    }

    public boolean EstaVacia() {
        return laCabeza == null;
    }

    // -----Imprime la lista-----
    void Print() {
        if (laCabeza != null) {

            Imprimir(laCabeza);
        } else {
            System.out.println("Lista Vacia");
        }
    }

    void Imprimir(Nodo m) {
        if (m != null) {
            m.Print();
            Imprimir(m.next);
        }
    }

    // -----Clase Nodo---------
    private class Nodo {
        public int elObjeto;
        public Nodo next;

        public Nodo(int nuevoObjeto, Nodo next) {
            this.elObjeto = nuevoObjeto;
            this.next = next;
        }

        void Print() {
            System.out.print("-" + elObjeto + "-");
        }
    }

}
