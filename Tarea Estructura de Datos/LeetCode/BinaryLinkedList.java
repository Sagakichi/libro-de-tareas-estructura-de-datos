public class BinaryLinkedList {
    // Ejercicio 1290 Convert Binary Number in a Linked List to Integer
    public class ListNode {
        int val;
        ListNode next;

        ListNode() {
        }

        ListNode(int val) {
            this.val = val;
        }

        ListNode(int val, ListNode next) {
            this.val = val;
            this.next = next;
        }
    }

    class Solution {
        public int getDecimalValue(ListNode head) {
            // Se hace que el numero resultante sea igual a la cabeza
            ListNode n = head;
            int number = 0;
            // Actualiza el resultado moviendolo uno a la izquierda y agregando el valor
            // actual.
            while (n != null) {
                number = (number << 1) | n.val;
                n = n.next;
            }
            return number;

        }
    }
}
