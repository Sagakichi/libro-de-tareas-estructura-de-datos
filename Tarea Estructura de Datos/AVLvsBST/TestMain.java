package BSTvsAVL;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class TestMain {

	public static void main(String[] args) {
		/*
		 * Escriba aqu� su c�digo
		 */
		AvlTree a = new AvlTree();
		BSTree b = new BSTree();
		List<Integer> datos1 = new ArrayList<>();
		List<Integer> datos2 = new ArrayList<>();
		long[][] tiempos = new long[3][2];
		long inicio = 0;
		long finish = 0;
		for (int i = 0; i < 100000; i++) {
			datos1.add((i * 2));
			datos2.add((i * 2) + 1);
		}
		Collections.shuffle(datos1);
		Collections.shuffle(datos2);
		inicio = System.nanoTime();
		for (int i = 0; i < datos1.size(); i++) {
			a.insert(datos1.get(i));
		}
		finish = System.nanoTime() - inicio;
		tiempos[0][0] = finish;
		inicio = System.nanoTime();
		for (int i = 0; i < datos1.size(); i++) {
			b.insert(datos1.get(i));
		}
		finish = System.nanoTime() - inicio;
		tiempos[0][1] = finish;

		inicio = System.nanoTime();
		for (int i = 0; i < datos1.size(); i++) {
			a.search(datos1.get(i));
		}
		finish = System.nanoTime() - inicio;
		tiempos[1][0] = finish;
		inicio = System.nanoTime();
		for (int i = 0; i < datos1.size(); i++) {
			b.search(datos1.get(i));
		}
		finish = System.nanoTime() - inicio;
		tiempos[1][1] = finish;

		inicio = System.nanoTime();
		for (int i = 0; i < datos2.size(); i++) {
			a.search(datos2.get(i));
		}
		finish = System.nanoTime() - inicio;
		tiempos[2][0] = finish;
		inicio = System.nanoTime();
		for (int i = 0; i < datos2.size(); i++) {
			b.search(datos2.get(i));
		}
		finish = System.nanoTime() - inicio;
		tiempos[2][1] = finish;

		System.out.println("Tiempos de insercion -> AVL:" + tiempos[0][0] + " BST:" + tiempos[0][1]);
		System.out.println("Tiempos de busqueda -> AVL:" + tiempos[1][0] + " BST:" + tiempos[1][1]);
		System.out.println("Tiempos de busqueda fallida -> AVL:" + tiempos[2][0] + " BST:" + tiempos[2][1]);

	}

}
