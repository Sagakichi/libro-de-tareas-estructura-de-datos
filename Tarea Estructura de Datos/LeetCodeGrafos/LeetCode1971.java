class Solution {
    boolean found = false;

    public boolean validPath(int n, int[][] edges, int start, int end) {
        if (start == end)
            return true;

        Map<Integer, List<Integer>> graph = new HashMap();
        boolean[] visited = new boolean[n];

        for (int i = 0; i < n; i++)
            graph.put(i, new ArrayList());
        // construye el grafo y agrega un vertice bidireccional
        for (int[] edge : edges) {
            graph.get(edge[0]).add(edge[1]);
            graph.get(edge[1]).add(edge[0]);
        }
        // Empieza la busqueda en profundidad desde el punto de inicio
        dfs(graph, visited, start, end);
        return found;
    }

    private void dfs(Map<Integer, List<Integer>> graph, boolean[] visited, int start, int end) {
        if (visited[start] || found)
            return;
        visited[start] = true;
        // Cuando encontramos al vecino que es igual al punto final dentro de la
        // recursividad, se rompe el for y devuelve true
        for (int nei : graph.get(start)) {
            if (nei == end) {
                found = true;
                break;
            }
            if (!visited[nei])
                dfs(graph, visited, nei, end); // en caso contrario seguira buscando
        }
    }
}