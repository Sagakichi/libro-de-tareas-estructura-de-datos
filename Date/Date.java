package Controlador;

public class Date {
    /*
     * data --> 32 bits
     * [26,31] --> 6 bits --> Minuto
     * [21,25] --> 5 bits --> Hora
     * [16,20] --> 5 bits --> Dia
     * [12,15] --> 4 bits --> Mes
     * [0,11] --> 12 bits --> Año
     */
    private int data;

    public Date() {
        data = 0;
    }

    public int getAnio() {
        int anio = data >>> 20;
        return anio;
    }

    public int getMes(){
        int mes = (data << 12) >>> 28;
        return mes;
    }

    public int getDia(){
        int dia = (data << 16) >>> 27;
        return dia;
    }

    public int getHora(){
        int hora = (data << 21) >>> 27;
        return hora;
    }

    public int getMinuto(){
        int minuto = (data << 26) >>> 26;
        return minuto;
    }

    public void setAnio (int anio){
        if (anio >=0 && anio < 4096){
            int mask = 8191; // 00000000000011111111111111111111
            data = (data & mask) | (anio << 20);
        }else{
            System.out.println("El anio no se ha podido reconocer");
        }
    }

    public void setMes(int mes){
        if (mes >= 0 && mes < 13){
            int mask = 2146439167 | (1 << 31);
            data = (data & mask) | (mes << 16);
        }else{
            System.out.println("El mes no se ha podido ingresar");
        }
    }

    public void setDia(int dia){
        if (dia > 0 && dia < 32){
            int mask = 2147479555 | (1 << 31);
            data = (data & mask) | (dia << 11);

        }else{
            System.out.println("Dia no ingresado");
        }
    }

    public void setHora(int hora){
        if (hora >= 0 && hora < 24){
            int mask = 992 | (1 << 31);
        }
    }

    public String toString(){
        return getDia() + "/" + getMes() + "/" + getAnio();
    }

}

