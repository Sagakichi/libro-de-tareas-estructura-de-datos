package Main;

import Controlador.Date;

import java.util.Scanner;

public class Main {
    public static void main(String[] args){
        Scanner tec = new Scanner(System.in);
        Date data = new Date();
        int anio, mes, dia;
        System.out.println("Ingrese año: ");
        anio = tec.nextInt();
        data.setAnio(anio);
        System.out.println("Ingrese mes: ");
        mes = tec.nextInt();
        data.setMes(mes);
        System.out.println("Ingrese dia: ");
        dia = tec.nextInt();
        data.setDia(dia);
        System.out.println(data.toString());

    }
}
