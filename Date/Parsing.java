import java.util.Stack;

import javax.lang.model.util.ElementScanner6;

/**
 * 
 * @author Martita
 * 
 *         Para m�s info, visitar
 *         https://es.stackoverflow.com/questions/26907/infijo-a-posfijo-en-java
 *         Y http://informatica.uv.es/iiguia/AED/laboratorio/P6/pr_06_2005.html
 *
 */

public class Parsing {

	public static String fromInfijoToPostfijo(String infijo) {
		String salida = "";
		Stack<Character> pila = new Stack<>();
		char[] charInfijo;
		charInfijo = infijo.toCharArray();
		for (char s : charInfijo) {
			if (isANumber(s)) {
				salida += s;
			} else {
				if (s == '(') {
					pila.push(s);
				} else {
					if (s == ')') {
						String pilax = "";
						char[] pilaArray = pilax.toCharArray();
						while (pila.peek() != '(') {
							pilax += pila.pop();
						}
						for (char c : pilaArray) {
							pila.push(c);
						}
					} else {
						if (!isANumber(s)) {
							if (pila.size() == 0) {
								pila.push(s);
							} else {
								if (prioridad(s) > prioridad(pila.peek())) {
									pila.push(s);
								} else {
									salida += pila.pop();
									pila.push(s);
								}
							}
						}
					}
				}
			}
		}
		while (pila.empty() == false) {
			salida += pila.pop();
		}
		return salida;
	}

	private static boolean isANumber(char symbol) {
		return symbol == '0' || symbol == '1' || symbol == '2' || symbol == '3' ||
				symbol == '4' || symbol == '5' || symbol == '6' || symbol == '7' ||
				symbol == '8' || symbol == '9';
	}

	private static int prioridad(char symbol) {
		if (symbol == '(')
			return 0;
		if (symbol == '+' || symbol == '-')
			return 1;
		if (symbol == '*' || symbol == '/')
			return 2;
		return -1;
	}

	public static ArbolExpresion getArbol(String postfijo) {
		Stack<ArbolExpresion> arbolito = new Stack<>();
		char[] posfijoCha = postfijo.toCharArray();
		for (char c : posfijoCha) {
			ArbolExpresion arbolazo = new ArbolExpresion(c);
			if (isANumber(c)) {
				arbolito.push(arbolazo);
			} else {
				arbolazo.left = arbolito.pop();
				arbolazo.right = arbolito.pop();
				arbolito.push(arbolazo);
			}
		}
		return arbolito.peek();
	}

}
